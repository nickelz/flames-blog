const mongoose = require('mongoose')
const moment = require('moment')

var ThreadSchema = new mongoose.Schema({
	author: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	
	title: {
		type: String,
		required: true,
		minlength: 4
	},

	body: {
		type: String,
		required: true
	},

	dateCreated: {
		type: Date,
		default: Date.now
	},

	comments: [{
		comment: {
			type: String,
			required: true,
		},

		postedBy: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User',
			required: true,
		},

		postTime: {
			type: Date,
			default: Date.now,
		},
	}]
})

module.exports = mongoose.model('Thread', ThreadSchema)