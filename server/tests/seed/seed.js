const { ObjectID } = require('mongodb')
const jwt = require('jsonwebtoken')

const Thread = require('./../../models/Thread')
const User = require('./../../models/User')

const userOneId = new ObjectID()
const userTwoId = new ObjectID()

const users = [
	{
		_id: userOneId,
		username: 'AliTest',
		email: 'alitest@gmail.com',
		password: '123123',
		tokens: [{
			access: 'auth',
			token: jwt.sign({
				_id: userOneId,
				access: 'auth'
			}, process.env.JWT_SECRET).toString()
		}]
	}, {
		_id: userTwoId,
		username: 'AhmedTest',
		email: 'ahmedtest@gmail.com',
		password: '121212',
		tokens: [{
			access: 'auth',
			token: jwt.sign({
				_id: userTwoId,
				access: 'auth'
			}, process.env.JWT_SECRET).toString()
		}]
	}
]

const threads = [
	{
		_id: new ObjectID(),
		author: userOneId,
		title: 'How to get away with murder',
		body: 'Just eat milk and you gon be invisible',
	}, {
		_id: new ObjectID(),
		author: userTwoId,
		title: 'Why would I even do that?',
		body: 'Because you just can, ya know :D',
	},
]

const populateThreads = (done) => {
	Thread.remove({ }).then(() => {
		return Thread.insertMany(threads)
	}).then(() => done())
}

const populateUsers = (done) => {
	User.remove({ }).then(() => {
		var userOne = new User(users[0]).save()
		var userTwo = new User(users[1]).save()

		return Promise.all([userOne, userTwo])
	}).then(() => done())
}

module.exports = {
	threads,
	populateThreads,
	users,
	populateUsers,
}