const expect = require('expect')
const request = require('supertest')
const {
	ObjectID
} = require('mongodb')

const {
	app
} = require('./../app')
const Thread = require('./../models/Thread')
const User = require('./../models/User')
const {
	threads,
	populateThreads,
	users,
	populateUsers,
} = require('./seed/seed')

beforeEach(populateUsers)
beforeEach(populateThreads)

describe('GET /threads', () => {
	it('should get all threads', (done) => {
		request(app)
			.get('/threads')
			.send({})
			.expect(200)
			.expect((res) => {
				expect(res.body.length).toBe(2)
			})
			.end(done)
	})
})

describe('POST /threads/new', () => {
	it('should create a new thread', (done) => {
		var newThread = {
			author: users[0]._id,
			title: 'How to SLAY Unit Testing',
			body: 'You have to be me first',
		}

		request(app)
			.post('/threads/new')
			.set('x-auth', users[0].tokens[0].token)
			.send(newThread)
			.expect(200)
			.expect((res) => {
				expect(res.body.title).toBe(newThread.title)
			})
			.end((err, res) => {
				if (err) return done(err)

				Thread.find({
					title: newThread.title
				}).then((threads) => {
					// console.log(threads)
					expect(threads.length).toBe(1)
					expect(threads[0].title).toBe(newThread.title)
					done()
				}).catch((e) => done(e))
			})
	})

	it('should not create a thread with invalid body data', (done) => {
		request(app)
		.post('/threads/new')
		.set('x-auth', users[0].tokens[0].token)
		.send({ })
		.expect(400)
		.end((err, res) => {
			if (err) return done(err)

			Thread.find().then((threads) => {
				expect(threads.length).toBe(2)
				done()
			}).catch((e) => done(e))
		})
	})
})

describe('GET /threads/:threadId', () => {
	it('should get a specific thread', (done) => {
		request(app)
		.get(`/threads/${threads[0]._id.toHexString()}`)
		.set('x-auth', users[1].tokens[0].token)
		.expect(200)
		.expect((res) => {
			expect(res.body.title).toBe(threads[0].title)
		})
		.end(done)
	})

	var hexID = new ObjectID().toHexString()

	it('should return 404 if thread not found', (done) => {
		request(app)
		.get(`/threads/${hexID}`)
		.expect(404)
		.end(done)
	})
})

