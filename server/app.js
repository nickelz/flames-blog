require('./config/config')

const express = require('express')
const bodyParser = require('body-parser')

var {
	create_new_user,
	get_list_of_users,
	user_logout,
	user_login
} = require('./controllers/UserC')

var {
	create_new_thread,
	list_all_threads,
	list_user_threads,
	read_thread,
	comment_on_thread,
} = require('./controllers/ThreadC')

var {
	authenticate
} = require('./middleware/authenticate')

var User = require('./models/User')

var app = express()
const port = process.env.PORT

// Configuration
app.use(bodyParser.json())

// Routing
app.route('/user')
	.post(create_new_user)
	.get(get_list_of_users)

app.route('/user/logout')
	.delete(authenticate, user_logout)

app.route('/user/login')
	.post(user_login)

app.route('/threads')
	.get(list_all_threads)

app.route('/threads/new')
	.post(authenticate, create_new_thread)

app.route('/threads/:threadId')
	.get(read_thread)
	.post(authenticate, comment_on_thread)

app.route('/threads/user/:username')
	.get(authenticate, list_user_threads)

app.listen(port, () => console.log(`Server running on port ${port} ..`))

module.exports = {
	app
}