const _ = require('lodash')
const {
	ObjectID
} = require('mongodb')

var Thread = require('./../models/Thread')
var User = require('./../models/User')

// POST /threads/new
var create_new_thread = (req, res) => {
	var body = _.pick(req.body, ['title', 'body'])

	var thread = new Thread({
		author: req.user._id,
		title: body.title,
		body: body.body
	})

	thread.save().then((thread) => {
		res.send(thread)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// GET /threads
var list_all_threads = (req, res) => {
	Thread.find().then((threads) => {
		res.send(threads)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// GET /threads/:username
var list_user_threads = (req, res) => {
	Thread.find().populate('author').exec(function (e, threads) {
		if (e) return res.status(400).send(e)

		var filteredArray = threads.filter((element) => element.author.username === req.params.username)
		res.send(filteredArray)
	})
}

// GET /threads/:threadId
var read_thread = (req, res) => {
	var id = req.params.threadId

	if (!ObjectID.isValid(id)) return res.status(404).send()

	Thread.findById(id).then((thread) => {
		if (!thread) return res.status(404).send()
		res.send(thread)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// POST /threads/:threadId
var comment_on_thread = (req, res) => {
	Thread.findByIdAndUpdate(req.params.threadId, {
		$push: {
			comments: {
				comment: req.body.comment,
				postedBy: req.user._id,
			},
		},
	}).then((thread) => {
		res.send(thread)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// Exports
module.exports = {
	create_new_thread,
	list_all_threads,
	list_user_threads,
	read_thread,
	comment_on_thread,
}