const _ = require('lodash')
const jwt = require('jsonwebtoken')

var { mongoose } = require('./../db/mongoose')
var User = require('./../models/User')

// POST /user
var create_new_user = (req, res) => {
	var body = _.pick(req.body, ['username', 'email', 'password'])
	var user = new User(body)

	user.save().then(() => {
		return user.generateAuthToken()
	}).then((token) => {
		res.header('x-auth', token).send(user)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// GET /user
var get_list_of_users = (req, res) => {
	User.find().then((users) => {
		res.status(200).send(users)
	})
}

// DELETE /user/logout
var user_logout = (req, res) => {
	req.user.removeToken(req.token).then(() => {
		res.status(200).send(`${req.user.username} has logged out`)
	}, () => {
		res.status(400).send('fail')
	})
}

// POST /user/login
var user_login = (req, res) => {
	var body = _.pick(req.body, ['email', 'password'])

	User.findByCredentials(body.email, body.password).then((user) => {
		return user.generateAuthToken().then((token) => {
			res.header('x-auth', token).send(`${user.username} has logged in`)
		})
	}).catch((e) => {
		res.status(400).send('fail')
	})
}

module.exports = {
	create_new_user,
	get_list_of_users,
	user_logout,
	user_login,
}